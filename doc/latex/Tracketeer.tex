\documentclass{llncs}
\usepackage[utf8]{inputenc}
\usepackage[style=authortitle,citestyle=authoryear,maxnames=10]{biblatex}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[pdftex]{graphicx} 

\author{120023600}
\title{Tracketeer - Pathfinding with RRTs and Potential Fields}
\institute{School of Computer Science, University of St Andrews}
\date{\today}

\bibliography{ref} 
\begin{document}

\maketitle

\begin{abstract}
This report describes and compares two different approaches, namely RRTs and potential fields, for pathfinding problems. Those methods are used in an application called \emph{Tracketeer} whose usage is explained as well as how it enables the user to follow the pathfinding process.
\end{abstract}

\section{Introduction}

Two approaches for pathfinding have been implemented by me in Java. Rapidly expanding Random Trees (RRT) and Potential Fields. Both of them are used in a common application called \emph{Tracketeer} to find a path through a course of obstacles and arrive at a certain goal position.

At first it will be shortly explained how \emph{Tracketeer} can be used.
After that details about how the different approaches work and perform with free space and obstacle navigation will be described.
Both of them will be compared and a recommendation is made on when to use which approach.

\section{Tracketeer UI}

Tracketeer is a robot motion planning and execution system.
Figure~\ref{fig:protfield-details} shows two windows. The top window is the application's main window, the execution window.

By pressing \emph{space} a plan will be created if not already present.
The same action makes the robot (black/white circle) move one step ahead into the direction of the goal.
The black line shows the path the robot has already put behind it.
The gray one shows the planned path to the goal.
At the end of the path its length in pixels is indicated.

The three most important controls at the bottom of the main window are:
\begin{itemize}
\item[PotField] This button toggles between the use of an ArbiTree (RRT) or Potential field for path planning.
\item[Spawn Search] The lower window in Figure~\ref{fig:protfield-details} is not visible per default. It will be opened when this button is clicked. It contains detailed information on how the path was planned. In this case with a Potential Field. The user can replay the planning with the use of the \emph{space} key and can also play around with the \emph{arrow} keys.
\item[Reset] This will reset the plan and obstacles\footnote{Obstacles can be removed by double clicking them in the main window.}. There is also a keyboard shortcut (R) for this.
\end{itemize}
Both the main window and the RRT details window support scrolling through dragging.

\begin{figure}[hbtp]
\centering
\includegraphics[scale=1, width=\textwidth]{potfield-details.png}
\caption{Tracketeer with expanded planning details.}
\label{fig:protfield-details}
\end{figure}

\section{Planning \& Execution}

\subsection{Free Space}

In free space travel potential fields beat RRTs easily. As can be seen in Figure~\ref{fig:freespace} the potential field plan will just go the shortest distance to the goal on a straight line.
The RRT however takes a longer than necessary\footnote{800 vs 600 px} path to find the goal.
Per default both planners simply use a step size of 10. This number is mostly arbitrary, though it must be smaller than the sample size of the potential field, which is 30 per default here.

\begin{figure}[hbtp]
\centering
\includegraphics[scale=1, width=\textwidth]{freespace.png}
\caption{Freespace travel both using a potential field and an RRT.}
\label{fig:freespace}
\end{figure}

The \emph{Potential Field} approach takes the straight line and hence about 60 steps to arrive at the goal.

The \emph{RRT} in Figure~\ref{fig:freespace} takes 113 steps and has a ratio of 6 of unused to used nodes. Adding goal bias to the RRT will reduce the number of steps significantly, because it will go more directly into the direction of the goal.
However, this only works safely in free space travel. If there are obstacles the completeness of the algorithm could be broken by using a goal bias\footnote{Note that goal bias is not implemented in this version of Tracketeer}.

In conclusion the potential field planner is to be preferred over the RRT planner, because it will definitely go the shortest path. Not only yields the potential field a shorter path but also will it do so in less time than the RRT. It moves straight to the goal at all times and does not unnecessarily explore other directions.

\subsection{Obstacle Navigation}

As both Figure~\ref{fig:protfield-details} and Figure~\ref{fig:rrt-details} show obstacle navigation works reasonably well with both potential fields and RRTs.

\subsubsection{Obstacle Sensing}

\begin{quote}
"What problems occur for RRT’s and Potential Fields when obstacles are not known except through sensing (and obstacle locations are not stored as behaviour proceeds)?"\footnote{MKW 5/10/12}
\end{quote}

\paragraph{Potential Field}
If the potential field is not given any obstacles at first it will start off by going directly towards the goal in a straight line. The robot would have to create a plan like this first and then check with every step if it will collide with an obstacle.
Encountered obstacles then have to be added to the potential field in order for it to be able to reach the goal at all. Without doing it a new plan would simply go into the same (dead-end) direction.

When a obstacle map is built up during execution it will be able to avoid that.
However, the robot could walk right into a next obstacle and be forced to turn around in order to avoid it\footnote{Similar to what can be seen in Figure~\ref{fig:protfield-details} where the path makes a u-turn.}.
It could lead to detours but in the end it would also find a path to the goal.

\paragraph{RRT}
The RRT in Tracketeer makes use of a global map of obstacles as to not extend the tree into obstructed areas. This means that it will always create a valid plan on the first try. Although it would make things more difficult it would also work without a global map.

In that case the robot would create plan through the RRT first without knowing if it is valid. It would then proceed to make one step after another while always checking (through sensing) if the next step ahead would lead into an obstacle.
If that is the case it would have to create a new plan with another RRT starting from its current position. These steps would have to be repeated until the goal is reached.

The issue with that is that it could lead to larger detours. Building up the map of obstacles during execution would mitigate this issue, but would still be worse than having a global map to begin with.

\paragraph{Implementation}

In this version of \emph{Tracketeer} global obstacle maps for both potential fields and RRTs are used. It would be trivial to add what is called "Fog" in the UI, though.
With more time it would have been implemented.

\begin{figure}[hbtp]
\centering
\includegraphics[scale=1, width=\textwidth]{rrt-details.png}
\caption{Tracketeer with expanded planning details.}
\label{fig:rrt-details}
\end{figure}

\subsection{Evaluation}

It may be apparent through comparison of Figure~\ref{fig:protfield-details} and ~\ref{fig:rrt-details} that RRTs can beat potential fields in more complex obstacle courses. In the courses I tried the RRTs would usually find shorter ways.
RRTs can especially recover better from dead-ends. While the potential field path would have to make a turn in a dead-end an RRT can possibly find a completely different path to avoid them in the first place.
However, if the courses are not that narrow or complicated the potential field approach can yield a shorter path.

A potential field is significantly faster in its computation and also consumes less memory than an RRT. But it may not find short paths for very complicated obstacle configurations.
Therefore I would recommend using RRTs for obstacle navigation if CPU and memory usage is no issue and potential fields if it is.

\section{Overview Questions}

\subsection{How would you develop your system to deal with the problems you found?}

The most important problem I found was that RRTs can take a lot of computation time especially when compared to potential fields.
I already have mitigated this a tiny bit by implementing a parallel RRT\footnote{Class tracketeer.search.arbitree.ParallelArbiTree} which makes use of all available cores for the nearest neighbour search.

However, this is still not optimal. A KD-tree would yield better results.

\subsection{What similarities and differences do these kinds of search have with symbolic AI search?}

The main difference between these kinds of searches and symbol search is, that in symbolic search all paths are in principle already known. Of that set of paths the right one "just" has to be picked.

With RRTs and potential fields the nodes are only discovered during planning.
Also while with symbol AI it is known if a found path is minimal this cannot be said for RRTs or potential fields.

\section{Conclusion}

In this report the pathfinding application \emph{Tracketeer} was described as well as the search algorithms used. Two different approaches have been compared in different scenarios and a recommendation has been made on which approach to use under which circumstances. To summarise: RRTs are more powerful than potential fields in very complex obstacle courses while also requiring significantly more resources.
Potential fields will also definitely find a path, though and that at a lesser cost.

\printbibliography 

\end{document}
