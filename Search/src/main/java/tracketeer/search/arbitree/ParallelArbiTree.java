package tracketeer.search.arbitree;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;

public class ParallelArbiTree extends ArbiTree {

    private volatile Point currentSamplePoint;
    private List<Search> searches;
    private int searchIndex = 0;
    private ExecutorService exec;

    public ParallelArbiTree(Point start, Rectangle sampleBounds, Set<Shape> obstacles, Shape target) {
        super(start, sampleBounds, obstacles, target);

        init();
    }

    public ParallelArbiTree(Point start, Set<Shape> obstacles, Shape target) {
        super(start, obstacles, target);

        init();
    }

    public ParallelArbiTree(Point start, Shape target) {
        super(start, target);

        init();
    }

    public void shutdown() {
        exec.shutdown();
    }

    protected void init() {
        int numProcessors = Runtime.getRuntime().availableProcessors();

        this.searches = new ArrayList<Search>(numProcessors);
        this.exec = Executors.newFixedThreadPool(numProcessors);

        for (int i = 0; i < numProcessors; ++i) {
            searches.add(new Search());
        }
    }

    @Override
    public Extension extend(Point point) {
        currentSamplePoint = point;

        return super.extend(point);
    }

    @Override
    protected void onExtension(Extension ext) {
        searches.get(searchIndex++ % searches.size()).add(ext.node);
    }

    @Override
    protected Node createRoot(Point start) {
        return new ParaNode(null, start);
    }

    class ParaNode extends Node {

        public ParaNode(Node parent, Point point) {
            super(parent, point);
        }

        @Override
        public Node getNearestNeighbour(Point point) {
            try {
                List<Future<SearchResult>> results = exec.invokeAll(searches);
                SearchResult result = new SearchResult(this, distance(point));

                for (Future<SearchResult> res : results) {
                    result = result.min(res.get());
                }

                if (result.node == null) {
                    return this;
                } else {
                    return result.node;
                }
            } catch (InterruptedException e) {
                System.err.println("Execution interrupted. Fallback to sequential nearest neighbour search.");

                return super.getNearestNeighbour(point);
            } catch (ExecutionException e) {
                System.err.println("Execution failed (" + e.getMessage() +
                        "). Fallback to sequential nearest neighbour search.");

                return super.getNearestNeighbour(point);
            }
        }
    }

    class Search implements Callable<SearchResult> {

        private final List<Node> nodes = new LinkedList<Node>();

        public Search() {

        }

        public SearchResult call() {
            Node nearestNode = null;
            int smallestDistance = 0;
            for (Node node : nodes) {
                int dist = node.distance(currentSamplePoint);
                if (SearchResult.min(smallestDistance, dist) == dist) {
                    nearestNode = node;
                    smallestDistance = dist;
                }
            }

            return new SearchResult(nearestNode, smallestDistance);
        }

        public void add(Node node) {
            nodes.add(node);
        }
    }
}
