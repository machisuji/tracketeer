package tracketeer.search.arbitree.visual;

import tracketeer.search.arbitree.ArbiTree;
import tracketeer.search.arbitree.ParallelArbiTree;
import tracketeer.search.geom2d.DrawableShape;
import tracketeer.search.geom2d.Rect;
import tracketeer.search.geom2d.Vector;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Iterator;
import java.util.List;

public class JArbiTree extends JComponent {

    private ArbiTree tree;
    private Point lastSample;
    private ArbiTree.Node lastNode;
    private volatile boolean bulk = false;
    private List<ArbiTree.Node> rootPath;

    private volatile Point dragStart;
    private volatile boolean dragging;
    private int tx, ty;

    public JArbiTree(ArbiTree tree) {
        this.tree = tree;

        if (tree.getConnectingExtension() != null) {
            rootPath = tree.getConnectingExtension().node.getRootPath();
        }

        tree.setArbiTreeListener(new ArbiTree.Listener() {
            public void nodeInserted(ArbiTree.Node node) {
                if (!bulk) {
                    repaint();
                }
                lastNode = node;
            }

            public void extendTowards(Point point) {
                lastSample = point;
            }
        });

        Controls ctrl = new Controls();

        addKeyListener(ctrl);
        addMouseMotionListener(ctrl);
        addMouseListener(ctrl);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g.setColor(new Color(80, 130, 50));
        g.fillRect(0, 0, getWidth(), getHeight());

        g.translate(tx, ty);

        Rectangle rect = tree.getSampleBounds();
        BasicStroke dashed = new BasicStroke(1f, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER, 10f, new float[] { 10f }, 0f);
        Stroke stroke = g.getStroke();

        g.setStroke(dashed);
        g.setColor(new Color(255, 130, 50));
        g.drawRect(rect.x, rect.y, rect.width, rect.height);

        if (tree.isGoalBias()) {
            g.setColor(new Color(255, 180, 80));
            Rectangle br = tree.getBiasedSampleBounds();

            g.drawRect(br.x, br.y, br.width, br.height);
        }

        g.setStroke(stroke);
        g.drawString("Sample Area", rect.x + rect.width, rect.y + rect.height + g.getFont().getSize());

        Point pos = tree.getStart();
        g.setColor(Color.RED);
        g.fillRect(pos.x, pos.y, 1, 1);
        g.drawOval(pos.x - 5, pos.y - 5, 10, 10);

        rect = tree.getTarget().getBounds();
        g.setColor(Color.GRAY);
        g.fillRect(rect.x, rect.y, rect.width, rect.height);

        if (lastSample != null && lastNode != null) {
            g.setColor(Color.BLUE);
            g.fillRect(lastSample.x, lastSample.y, 1, 1);
            g.drawOval(lastSample.x - 5, lastSample.y - 5, 10, 10);

            g.drawLine(lastNode.getPoint().x, lastNode.getPoint().y,
                    lastNode.getParent().getPoint().x, lastNode.getParent().getPoint().y);
        }

        g.setColor(Color.MAGENTA);
        for (Shape shape : getTree().getObstacles()) {
            if (shape instanceof DrawableShape) {
                ((DrawableShape) shape).drawOn(g);
            } else {
                Rectangle r = shape.getBounds();
                g.drawRect(r.x, r.y, r.width, r.height);
            }
        }

        g.setColor(Color.BLACK);
        connect(tree.getRoot(), g);

        if (rootPath != null) {
            Iterator<ArbiTree.Node> path = rootPath.iterator();
            ArbiTree.Node start = path.next();

            g.setColor(Color.PINK);
            while (path.hasNext()) {
                ArbiTree.Node end = path.next();
                g.drawLine(start.getPoint().x, start.getPoint().y,
                        end.getPoint().x, end.getPoint().y);

                start = end;
            }

            int numNodes = tree.size();
            int pathLength = rootPath.size();

            double ratio = numNodes / (double) pathLength;

            g.setColor(Color.WHITE);
            g.drawString(
                    String.format("Ratio: %.2f (%d:%d)", ratio, numNodes, pathLength),
                    start.getPoint().x, start.getPoint().y - g.getFont().getSize());
        }
    }

    private void connect(ArbiTree.Node node, Graphics g) {
        Point start = node.getPoint();

        for (ArbiTree.Node end : node.getChildren()) {
            g.drawLine(start.x, start.y, end.getPoint().x, end.getPoint().y);
            connect(end, g);
        }
    }

    public ArbiTree getTree() {
        return tree;
    }

    public static JFrame showArbiTree(ArbiTree tree) {
        JArbiTree tr = new JArbiTree(tree);
        JFrame frame = new JFrame("Showing ArbiTree " + tree);

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationByPlatform(true);
        frame.setSize(640, 480);
        frame.setContentPane(tr);

        frame.setVisible(true);

        tr.requestFocus();

        return frame;
    }

    public static void main(String[] args) {
        ArbiTree tree = new ParallelArbiTree(new Vector(150, 150), new Rect(340, 400, 20, 20));

        tree.getObstacles().add(new Rect(300, 300, 120, 80).rotate(new Vector(300, 300), Math.toRadians(-45)));
        tree.getObstacles().add(new Rect(200, 220, 40, 100));

        JArbiTree.showArbiTree(tree).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    class Controls implements KeyListener, MouseMotionListener, MouseListener {

        public void keyTyped(KeyEvent keyEvent) { }

        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                ArbiTree.Extension ext = getTree().extend();
                if (ext.inTarget) {
                    rootPath = ext.node.getRootPath();
                }
            }
        }

        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_R && e.isControlDown()) {
                int iterations = 0;
                ArbiTree.Extension ext;
                bulk = true;
                do {
                    ext = getTree().extend();
                    ++iterations;
                } while (!ext.inTarget);
                rootPath = ext.node.getRootPath();
                repaint();
                bulk = false;
                JOptionPane.showMessageDialog(JArbiTree.this, "Reached target after " + iterations + " iterations");
            }
        }

        public void mouseDragged(MouseEvent e) {
            tx = e.getX() - dragStart.x;
            ty = e.getY() - dragStart.y;

            repaint();
        }

        public void mouseMoved(MouseEvent e) {

        }

        public void mouseClicked(MouseEvent e) {

        }

        public void mousePressed(MouseEvent e) {
            if (!dragging) {
                dragging = true;
                dragStart = e.getPoint();
            }
        }

        public void mouseReleased(MouseEvent e) {
            dragging = false;
        }

        public void mouseEntered(MouseEvent e) {

        }

        public void mouseExited(MouseEvent e) {

        }
    }
}
