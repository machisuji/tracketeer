package tracketeer.search.arbitree;

import java.awt.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Just a benchmark to see if the ParallelArbiTree is any good.
 * Example result below run on a 2Ghz Intel Core i7.
 *
 * Nearest Neighbour:
 *
 * Avg Time for         ArbiTree (0):       458115 ns (648 %)
 * Avg Time for ParallelArbiTree (1):        70724 ns (100 %)
 *
 * Random (path finding) scenarios:
 *
 * Seq Avg Time: 121 ms
 * Par Avg Time: 57 ms
 */
public class Benchmark {

    public static void main(String[] args) {
        testNearestNeighbour();
        System.out.println("\nTesting random scenarios ...\n");
        testRandomScenarios();
    }

    public static void testNearestNeighbour() {
        ArbiTree seqTree = createNormalTree();
        ParallelArbiTree parTree = createParaTree();

        System.out.print("Nearest Neighbour Setup ... ");
        for (int i = 0; i < 5000; ++i) {
            Point point = seqTree.randomPoint();

            seqTree.extend(point);
            parTree.extend(point);
        }
        System.out.println("complete.\n");
        test(Arrays.asList(seqTree, parTree), 5000);
    }

    public static void test(List<ArbiTree> trees, int rounds) {
        ArbiTree gen = trees.get(0);
        long[] times = new long[trees.size()];

        for (int r = 0; r < rounds; ++r) {
            Point sample = gen.randomPoint();
            for (int i = 0; i < trees.size(); ++i) {
                ArbiTree tree = trees.get(i);
                long ns = System.nanoTime();

                tree.getRoot().getNearestNeighbour(sample);
                ns = System.nanoTime() - ns;
                times[i] += ns;
            }
        }

        double minTime = Double.MAX_VALUE;
        for (long time : times) {
            double t = time / rounds;
            if (t < minTime) {
                minTime = t;
            }
        }

        for (int i = 0; i < times.length; ++i) {
            long time = times[i] / rounds;
            long part = Math.round((time / minTime) * 100d);
            System.out.printf("Avg Time for %16s (%d): %12d ns (%3d %%)%n",
                    trees.get(i).getClass().getSimpleName(), i, time, part);

            if (trees.get(i) instanceof ParallelArbiTree) {
                ((ParallelArbiTree) trees.get(i)).shutdown();
            }
        }
    }

    public static void testRandomScenarios() {
        List<Long> iterations = new LinkedList<Long>();
        List<Long> paraIterations = new LinkedList<Long>();
        List<Long> times = new LinkedList<Long>();
        List<Long> paraTimes = new LinkedList<Long>();
        final int ROUNDS = 42;
        long time = 0;

        System.out.println();
        for (int i = 0; i < ROUNDS; ++i) {
            System.out.print("|");
        }
        System.out.println();
        for (int i = 0; i < ROUNDS; ++i) {
            ArbiTree tree = createNormalTree();
            long ms = System.currentTimeMillis();
            long its = tree.findPath();
            ms = (System.currentTimeMillis() - ms);

            time += ms;
            iterations.add(its);
            times.add(ms);
            System.out.print("=");
        }
        System.out.println();
        long paraTime = 0;
        for (int i = 0; i < ROUNDS; ++i) {
            ParallelArbiTree tree = createParaTree();
            long ms = System.currentTimeMillis();
            long its = tree.findPath();
            ms = (System.currentTimeMillis() - ms);

            paraTime += ms;
            paraIterations.add(its);
            paraTimes.add(ms);
            System.out.print("=");
            tree.shutdown();
        }
        System.out.println();

        System.out.println("Seq Avg Time: " + (time / ROUNDS) + " ms");
        System.out.println("Par Avg Time: " + (paraTime / ROUNDS) + " ms");
        System.out.println();
        System.out.println("Seq Iterations: " + iterations);
        System.out.println("Par Iterations: " + paraIterations);
        System.out.println();
        System.out.println("Seq Times: " + times);
        System.out.println("Par Times: " + paraTimes);
    }

    public static ArbiTree createNormalTree() {
        ArbiTree tree = new ArbiTree(new Point(200, 200), new Rectangle(400, 400, 20, 20));

        tree.getObstacles().add(new Rectangle(300, 300, 120, 80));
        tree.getObstacles().add(new Rectangle(200, 220, 40, 100));

        return tree;
    }

    public static ParallelArbiTree createParaTree() {
        ParallelArbiTree tree = new ParallelArbiTree(new Point(200, 200), new Rectangle(400, 400, 20, 20));

        tree.getObstacles().add(new Rectangle(300, 300, 120, 80));
        tree.getObstacles().add(new Rectangle(200, 220, 40, 100));

        return tree;
    }
}
