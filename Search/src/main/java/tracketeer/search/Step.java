package tracketeer.search;

import tracketeer.search.geom2d.Vector;

import java.util.List;

public abstract class Step {

    private Vector position;
    private boolean inTarget;

    public Step(Vector position, boolean inTarget) {
        this.position = position;
        this.inTarget = inTarget;
    }

    public abstract List<Vector> getPath();

    public Vector getPosition() {
        return position;
    }

    public boolean isInTarget() {
        return inTarget;
    }
}
