package tracketeer.search;

import tracketeer.search.arbitree.ArbiTree;
import tracketeer.search.arbitree.ParallelArbiTree;
import tracketeer.search.arbitree.visual.JArbiTree;
import tracketeer.search.geom2d.DrawableShape;
import tracketeer.search.geom2d.Poly;
import tracketeer.search.geom2d.Rect;
import tracketeer.search.geom2d.Vector;
import tracketeer.search.potfield.PotentialField;
import tracketeer.search.potfield.visual.JPotField;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

public class World extends JComponent {

    private Vector startPosition;
    private Shape target;

    private Set<Shape> obstacles = new HashSet<Shape>();
    private Search search;
    private Vector lastStep;
    private boolean useArbiTree = false;

    private boolean fogOfWar = false;
    private Set<Shape> visibleObstacles = new HashSet<Shape>();

    private List<Vector> path = new LinkedList<Vector>();
    private List<Vector> plan = new LinkedList<Vector>();

    private double totalPlanCost = 0;

    private volatile boolean loading = false;

    private volatile Point dragStart;
    private volatile boolean dragging;
    private int tx, ty;

    public World() {
        addKeyListener(new Controls());

        MouseControls mc = new MouseControls();
        addMouseListener(mc);
        addMouseMotionListener(mc);

        setStartPosition(new Vector(50, 50));
        target = new Rect(560, 370, 25, 25);

        getObstacles().add(new Rect(300, 300, 120, 80).rotate(new Vector(300, 300), Math.toRadians(-45)));
        getObstacles().add(new Rect(200, 220, 40, 100));
        getObstacles().add(new Rect(270, 80, 200, 100));
        getObstacles().add(new Poly(
                new Vector(400, 430), new Vector(470, 360), new Vector(495, 415)));

        search = createSearch(getStartPosition(), target);

        search.getObstacles().addAll(getObstacles());
    }

    public static void main(String[] args) {
        World world = new World();

        showWorld(world);
    }

    public static JFrame showWorld(final World world) {
        JFrame frame = new JFrame("Tracketeer");
        JPanel content = new JPanel();
        JPanel buttons = new JPanel();

        final JButton reset = new JButton("Reset");
        reset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                world.reset();
            }
        });

        final JButton fog = new JButton("Fog");
        fog.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                world.setFogOfWar(!world.isFogOfWar());

                world.search.getObstacles().clear();
                if (!world.isFogOfWar()) {
                    world.search.getObstacles().addAll(world.getObstacles());
                }

                world.repaint();
                world.requestFocus();

                if (world.isFogOfWar()) {
                    fog.setText("Fog ✔");
                } else {
                    fog.setText("Fog");
                }
            }
        });
        fog.setEnabled(false);

        final JButton switchSearch = new JButton("PotField");
        switchSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                world.setUseArbiTree(!world.isUseArbiTree());
                world.resetSearch();

                if (world.isUseArbiTree()) {
                    switchSearch.setText("ArbiTree");
                    switchSearch.setToolTipText("Click to change to PotentialField");

                    fog.setEnabled(true);
                    world.repaint();
                } else {
                    switchSearch.setText("PotField");
                    switchSearch.setToolTipText("Click to change to ArbiTree");

                    world.setFogOfWar(false);
                    fog.setText("Fog");
                    fog.setEnabled(false);
                    world.repaint();
                }

                world.requestFocus();
            }
        });

        final JButton showSearch = new JButton("Spawn Search");
        showSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (world.search instanceof ArbiTree) {
                    ArbiTree tree = (ArbiTree) world.search;

                    JArbiTree.showArbiTree(tree);
                } else if (world.search instanceof PotentialField) {
                    PotentialField field = (PotentialField) world.search;
                    PotentialField copy = new PotentialField(
                            field.getStartPosition(), field.getObstacles(), field.getTarget());
                    JPotField.showPotentialField(copy);
                }
            }
        });

        final JButton bias = new JButton("Goal Bias");
        bias.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (world.search instanceof ArbiTree) {
                    ArbiTree tree = (ArbiTree) world.search;

                    if (tree.isGoalBias()) {
                        bias.setText("Goal Bias");
                    } else {
                        bias.setText("Goal Bias ✔");
                    }
                    tree.setGoalBias(!tree.isGoalBias());
                    world.repaint();
                    world.requestFocus();
                }
            }
        });
        bias.setEnabled(false);
        bias.setToolTipText("Does not work, yet");

        buttons.setLayout(new GridLayout());
        buttons.add(switchSearch);
        buttons.add(showSearch);
        buttons.add(reset);
        buttons.add(fog);
        buttons.add(bias);
        buttons.setBorder(new MatteBorder(2, 0, 0, 0, Color.WHITE));

        content.setLayout(new BorderLayout());

        content.add(world, BorderLayout.CENTER);
        content.add(buttons, BorderLayout.SOUTH);

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationByPlatform(true);
        frame.setSize(640, 480);
        frame.setContentPane(content);
        frame.setBackground(world.getBackgroundColor());

        frame.addWindowFocusListener(new WindowFocusListener() {
            public void windowGainedFocus(WindowEvent windowEvent) {
                world.requestFocus();
            }

            public void windowLostFocus(WindowEvent windowEvent) {

            }
        });

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (world.search instanceof ParallelArbiTree) {
                    ((ParallelArbiTree) world.search).shutdown();
                }
            }
        });

        frame.setVisible(true);

        world.requestFocus();

        return frame;
    }

    protected void reset() {
        lastStep = null;
        plan.clear();
        path.clear();

        resetSearch();
        repaint();
        requestFocus();
    }

    protected void plan() {
        Step step = search.finalStep();

        lastStep = step.getPosition();
        plan.addAll(step.getPath());

        totalPlanCost = getPlanCost();
    }

    protected void execute(int steps) {
        Iterator<Vector> nextSteps = plan.iterator();
        for (int i = 0; i < steps; ++i) {
            lastStep = nextSteps.next();
            path.add(lastStep);
            repaint();

            nextSteps.remove();

            if (steps > 1) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) { }
            }
        }
    }

    protected Search createSearch(Point start, Shape target) {
        if (isUseArbiTree()) {
            ArbiTree tree = new ParallelArbiTree(start, target);
            tree.setSampleBounds(getBounds());

            return tree;
        } else {
            return new PotentialField(start, target);
        }
    }

    protected void resetSearch() {
        Set<Shape> obstacles = isFogOfWar() ? new HashSet<Shape>() : getObstacles();

        if (search instanceof ParallelArbiTree) {
            ((ParallelArbiTree) search).shutdown();
        }

        search = createSearch(getStartPosition(), getTarget());
        search.getObstacles().addAll(obstacles);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.translate(tx, ty);

        paintObstacles(g);
        paintTarget(g);

        paintPath(g);
        paintPlan(g);

        paintRobot(g);
        paintTarget(g);

        if (loading) {
            paintLoading(g);
        }
    }

    public Color getBackgroundColor() {
        return new Color(50, 75, 35);
    }

    protected void paintLoading(Graphics2D g) {
        g.setColor(new Color(90, 20, 10, 150));
        g.fillRect(0, 0, getWidth(), getHeight());

        Font font = g.getFont();
        Font large = new Font("monospaced", Font.BOLD, 64);

        g.setFont(large);
        g.setColor(Color.WHITE);
        g.drawString("Loading ...", getWidth() / 2 - 128, getHeight() / 2 - 32);
        g.setFont(font);
    }

    protected void paintPath(Graphics2D g) {
        if (lastStep != null) {
            Iterator<Vector> path = this.path.iterator();
            if (path.hasNext()) {
                Vector start = path.next();

                g.setColor(Color.BLACK);
                while (path.hasNext()) {
                    Vector next = path.next();
                    g.drawLine(start.x, start.y, next.x, next.y);

                    start = next;
                }

                paintCost(getPathCost(), start, g);
            }
        }
    }

    protected void paintPlan(Graphics2D g) {
        BasicStroke dashed = new BasicStroke(1f, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER, 10f, new float[] { 10f }, 0f);
        Stroke stroke = g.getStroke();

        g.setColor(new Color(150, 150, 150, 150));
        g.setStroke(dashed);

        Iterator<Vector> path = plan.iterator();
        if (path.hasNext()) {
            Vector start = path.next();

            while (path.hasNext()) {
                Vector next = path.next();
                g.drawLine(start.x, start.y, next.x, next.y);

                start = next;
            }

            paintCost(totalPlanCost, start, g);
        }

        g.setStroke(stroke);
    }

    protected void paintCost(double cost, Vector pos, Graphics2D g) {
        Font font = g.getFont();
        g.setFont(new Font("monospaced", Font.ITALIC, 10));
        g.setColor(Color.WHITE);
        g.drawString(String.format("%.0f px", cost), pos.x + 5, pos.y - 10);
        g.setFont(font);
    }

    protected void paintRobot(Graphics2D g) {
        Point pos = lastStep != null ? lastStep : getStartPosition();

        g.setColor(Color.RED);
        g.fillOval(getStartPosition().x - 2, getStartPosition().y - 2, 4, 4);

        g.setColor(Color.BLACK);
        g.fillOval(pos.x - 5, pos.y - 5, 10, 10);
        g.setColor(Color.WHITE);
        g.fillOval(pos.x - 4, pos.y - 4, 8, 8);
        g.setColor(Color.BLACK);
        g.fillOval(pos.x - 3, pos.y - 3, 6, 6);
    }

    protected void paintObstacles(Graphics2D g) {
        g.setColor(Color.WHITE);

        Set<Shape> obstacles = search.getObstacles();
        for (Shape ob : obstacles) {
            drawShape(ob, g);
        }
    }

    protected void paintTarget(Graphics2D g) {
        g.setColor(Color.GREEN);
        drawShape(getTarget(), g);
    }

    protected void drawShape(Shape shape, Graphics2D g) {
        if (shape instanceof DrawableShape) {
            ((DrawableShape) shape).drawOn(g);
        } else {
            Rectangle rect = shape.getBounds();
            g.drawRect(rect.x, rect.y, rect.width, rect.height);
        }
    }

    public double getPlanCost() {
        return getCost(plan);
    }

    public double getPathCost() {
        return getCost(path);
    }

    protected double getCost(Collection<Vector> vertices) {
        Iterator<Vector> vs = vertices.iterator();
        if (vs.hasNext()) {
            double cost = 0;
            Vector a = vs.next();

            while (vs.hasNext()) {
                Vector b = vs.next();
                cost += a.distance(b);

                a = b;
            }

            return cost;
        } else {
            return -1;
        }
    }

    public Set<Shape> getObstacles() {
        return obstacles;
    }

    public Shape getTarget() {
        return target;
    }

    public Vector getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(Vector startPosition) {
        this.startPosition = startPosition;
    }

    public Set<Shape> getVisibleObstacles() {
        return visibleObstacles;
    }

    public boolean isUseArbiTree() {
        return useArbiTree;
    }

    public void setUseArbiTree(boolean useArbiTree) {
        this.useArbiTree = useArbiTree;
    }

    public boolean isFogOfWar() {
        return fogOfWar;
    }

    public void setFogOfWar(boolean fogOfWar) {
        this.fogOfWar = fogOfWar;
    }

    class Controls implements KeyListener {

        public void keyTyped(KeyEvent e) {

        }

        public void keyReleased(KeyEvent e) {
        }

        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                if (isGoalReached()) {
                    return;
                }
                if (plan.isEmpty()) {
                    loading = true;
                    World.this.paintImmediately(getBounds());

                    plan();

                    loading = false;
                    World.this.paintImmediately(getBounds());
                }
                execute(1);
            } else if (e.getKeyCode() == KeyEvent.VK_R) {
                reset();
            }
        }
    }

    class MouseControls extends MouseAdapter implements MouseMotionListener {

        public void mouseDragged(MouseEvent e) {
            tx = e.getX() - dragStart.x;
            ty = e.getY() - dragStart.y;

            repaint();
        }

        public void mousePressed(MouseEvent e) {
            if (!dragging) {
                dragging = true;
                dragStart = e.getPoint();
            }
        }

        public void mouseReleased(MouseEvent e) {
            dragging = false;
        }

        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                Shape obstacle = null;
                Point pos = new Point(e.getPoint().x - tx, e.getPoint().y - ty);
                for (Shape obs : search.getObstacles()) {
                    if (obs.contains(pos)) {
                        obstacle = obs;
                        break;
                    }
                }

                if (obstacle != null && search != null) {
                    search.getObstacles().remove(obstacle);
                    repaint();
                }
            }
        }
    }

    protected boolean isGoalReached() {
        return lastStep != null && target.contains(lastStep);
    }
}
