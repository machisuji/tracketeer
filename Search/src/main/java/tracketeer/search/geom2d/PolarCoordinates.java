package tracketeer.search.geom2d;

import java.awt.Point;

public interface PolarCoordinates<G extends PolarCoordinates<G>> {
    double getRadius();
    double getTheta();

    G rotate(double theta);
    G rotate(Point origin, double theta);

    G moveTo(double radius, double theta);
    G moveTo(Point origin, double radius, double theta);
}
