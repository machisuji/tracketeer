package tracketeer.search.geom2d;

import java.awt.*;

public class Vector extends Point implements PolarCoordinates<Vector>, Drawable {

    public Vector(int x, int y) {
        super(x, y);
    }

    public Vector(Point point) {
        this(point.x, point.y);
    }

    public Vector(Point start, Point end) {
        this(end.x - start.x, end.y - start.y);
    }

    /**
     * Creates a new Vector based on the given polar coordinates.
     *
     * @param r The radius (distance from origin).
     * @param theta The polar angle in radians.
     */
    public Vector(double r, double theta) {
        moveTo(r, theta);
    }

    public Vector(Point origin, double r, double theta) {
        moveTo(origin, r, theta);
    }

    public Vector scale(double factor) {
        setLocation(getX() * factor, getY() * factor);

        return this;
    }

    public Vector add(Point that) {
        setLocation(this.getX() + that.getX(), this.getY() + that.getY());

        return this;
    }

    public Vector heading(Vector b) {
        return Geom2D.heading(this, b);
    }

    public double distance(Vector that) {
        return Geom2D.distance(this, that);
    }

    public double length() {
        return Geom2D.length(this);
    }

    public Vector length(double length) {
        double x = getX();
        double y = getY();
        double l = Geom2D.length(this);

        x /= l; y /= l;
        x *= length; y *= length;

        setLocation(x, y);

        return this;
    }

    public Line connect(Vector that) {
        return new Line(this.copy(), that.copy());
    }

    /**
     * Polar coordinate angle theta.
     */
    public double getTheta() {
        return getTheta(getRadius());
    }

    /**
     * Polar coordinate angle theta.
     */
    public double getTheta(double r) {
        return Math.acos(x / r);
    }

    /**
     * Polar coordinate radius r.
     */
    public double getRadius() {
        return Math.sqrt(x * x + y * y);
    }

    public Vector rotate(double theta) {
        rotate(getRadius(), theta);

        return this;
    }

    public Vector rotate(Point origin, double theta) {
        setLocation(x - origin.x, y - origin.y);
        rotate(theta);
        setLocation(x + origin.x, y + origin.y);

        return this;
    }

    /**
     *
     * @param r
     * @param theta
     * @TODO yields unexpected results in some corner cases; fix that
     */
    protected void rotate(double r, double theta) {
        double theta1 = getTheta(r);
        double theta2 = theta1 + theta;

        setLocation(
                r * Math.cos(theta2),
                r * Math.sin(theta2));
    }

    public Vector moveTo(Point origin, double radius, double theta) {
        setLocation(0, 0);
        setLocation(radius * Math.cos(theta), radius * Math.sin(theta));
        setLocation(x + origin.x, y + origin.y);

        return this;
    }

    public Vector moveTo(double radius, double theta) {
        return moveTo(new Point(0, 0), radius, theta);
    }

    public Vector copy() {
        return new Vector(x, y);
    }

    @Override
    public String toString() {
        return String.format("Vector[x=%d,y=%d]", x, y);
    }

    public void drawOn(Graphics2D g) {
        g.drawRect(x, y, 1, 1);
    }
}
