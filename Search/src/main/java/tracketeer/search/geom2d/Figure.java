package tracketeer.search.geom2d;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class Figure<F extends Figure<F>>
        extends Polygon
        implements PolarCoordinates<F>, CenterPoint, DrawableShape {

    private Vector[] vectors;

    public Figure(Vector... vectors) {
        this.vectors = vectors;
    }

    public Vector[] getVectors() {
        return vectors;
    }

    @Override
    public void reset() {
        super.reset();

        for (Vector v : vectors) {
            addPoint(v.x, v.y);
        }
    }

    public double getRadius() {
        return vectors[0].getRadius();
    }

    public double getTheta() {
        return vectors[0].getTheta();
    }

    @SuppressWarnings("unchecked")
    public F rotate(double theta) {
        for (Vector v : vectors) {
            v.rotate(theta);
        }
        reset();

        return (F) this;
    }

    @SuppressWarnings("unchecked")
    public F rotate(Point origin, double theta) {
        for (Vector v : vectors) {
            v.rotate(origin, theta);
        }
        reset();

        return (F) this;
    }

    @SuppressWarnings("unchecked")
    public F moveTo(Point origin, double radius, double theta) {
        for (Vector v : vectors) {
            v.moveTo(v, radius, theta);
        }
        reset();

        return (F) this;
    }

    public F moveTo(double radius, double theta) {
        return moveTo(new Point(0, 0), radius, theta);
    }

    public Point getCenter() {
        Point center = new Point();
        center.setLocation(getBounds().getCenterX(), getBounds().getCenterY());

        return center;
    }

    public List<Vector> intersections(Line line) {
        List<Vector> results = new LinkedList<Vector>();
        Vector a, b;
        for (int i = 1; i <= vectors.length; ++i) {
            a = vectors[i - 1];
            b = vectors[i % vectors.length];

            Line seg = new Line(a, b);
            Point intersection = Geom2D.intersection(line, seg);

            if (intersection != null) {
                results.add(new Vector(intersection));
            }
        }
        return results;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(getClass().getSimpleName() + "(");
        boolean first = true;

        for (Vector v : vectors) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            sb.append(v.toString());
        }
        sb.append(")");

        return sb.toString();
    }

    public void drawOn(Graphics2D g) {
        g.drawPolygon(this);
    }
}
