package tracketeer.search.geom2d;

import java.awt.Graphics2D;

public interface Drawable {
    void drawOn(Graphics2D g);
}
