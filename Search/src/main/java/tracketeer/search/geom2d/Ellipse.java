package tracketeer.search.geom2d;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Ellipse extends Ellipse2D.Double implements DrawableShape, PolarCoordinates<Ellipse> {

    private Vector center;

    public Ellipse(Point center, int width, int height) {
        super(center.x - width / 2d, center.y - height / 2d, width, height);

        this.center = new Vector(center);
    }

    public Ellipse(int x, int y, int width, int height) {
        super(x, y, width, height);

        this.center = new Vector(x - width / 2, y - height / 2);
    }

    public void drawOn(Graphics2D g) {
        g.draw(this);
    }

    public double getRadius() {
        return getCenter().getRadius();
    }

    public double getTheta() {
        return getCenter().getTheta();
    }

    public Ellipse rotate(double theta) {
        return rotate(new Point(0, 0), theta);
    }

    public Ellipse rotate(Point origin, double theta) {
        Vector c = getCenter();

        c.rotate(origin, theta);
        setFrame(c.x - getWidth() / 2, c.y - getHeight() / 2, getWidth(), getHeight());

        return this;
    }

    public Ellipse moveTo(double radius, double theta) {
        return moveTo(new Point(0, 0), radius, theta);
    }

    public Ellipse moveTo(Point origin, double radius, double theta) {
        Vector c = new Vector(origin, radius, theta);

        this.center = c;
        setFrame(c.x - getWidth() / 2, c.y - getHeight() / 2, getWidth(), getHeight());

        return this;
    }

    public Vector getCenter() {
        return center;
    }
}
