package tracketeer.search.geom2d;

import java.awt.*;
import java.awt.geom.Line2D;

public class Line extends Line2D.Double implements PolarCoordinates<Line>, CenterPoint, Drawable {

    private Vector start;
    private Vector end;

    public Line(Vector start, Vector end) {
        super(start, end);

        this.start = start;
        this.end = end;
    }

    public Line(Point start, Point end) {
        this(new Vector(start), new Vector(end));
    }

    public Line(Point start, Vector v) {
        this(new Vector(start), v.add(start));
    }

    public Vector getStart() {
        return start;
    }

    public Vector getEnd() {
        return end;
    }

    public double getRadius() {
        return start.getRadius();
    }

    public double getTheta() {
        return start.getTheta();
    }

    public Line rotate(double theta) {
        start.rotate(theta);
        end.rotate(theta);

        return this;
    }

    public Line rotate(Point origin, double theta) {
        start.rotate(origin, theta);
        end.rotate(origin, theta);

        return this;
    }

    public Line moveTo(Point origin, double radius, double theta) {
        start.moveTo(origin, radius, theta);
        end.moveTo(origin, radius, theta);

        return this;
    }

    public Line moveTo(double radius, double theta) {
        return moveTo(new Point(0, 0), radius, theta);
    }

    public Line length(double length) {
        end.add(Geom2D.heading(start, end, length));

        return this;
    }

    @Override
    public String toString() {
        return String.format("Line[(%d, %d) => (%d, %d)]", start.x, start.y, end.x, end.y);
    }

    public Point getCenter() {
        return Geom2D.heading(start, end).scale(Geom2D.distance(start, end) * 0.5);
    }

    public Line copy() {
        return new Line(start, end);
    }

    public void drawOn(Graphics2D g) {
        g.drawLine(start.x, start.y, end.x, end.y);
    }
}
