package tracketeer.search.geom2d;

import java.awt.Shape;

public interface DrawableShape extends Shape, Drawable {
}
