package tracketeer.search.geom2d;

import java.awt.Point;

public interface CenterPoint {
    Point getCenter();
}
