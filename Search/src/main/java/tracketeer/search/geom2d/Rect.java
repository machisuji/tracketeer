package tracketeer.search.geom2d;

import java.awt.*;

public class Rect extends Figure<Rect> {

    private int width;
    private int height;

    public Rect(Rectangle bounds) {
        super(
            new Vector(bounds.x, bounds.y),
            new Vector(bounds.x + bounds.width, bounds.y),
            new Vector(bounds.x + bounds.width, bounds.y + bounds.height),
            new Vector(bounds.x, bounds.y + bounds.height));

        this.width = bounds.width;
        this.height = bounds.height;

        reset();
    }

    public Rect(Point center, double theta, int width, int height) {
        this(new Rectangle(new Point(center.x - width / 2, center.y - height / 2), new Dimension(width, height)));

        rotate(center, theta);
    }

    public Rect(int x, int y, int width, int height) {
        this(new java.awt.Rectangle(x, y, width, height));
    }

    public Point getLocation() {
        return getVectors()[0];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Rect copy() {
        return new Rect(getCenter(), getTheta(), getWidth(), getHeight());
    }

    @Override
    public Point getCenter() {
        Vector diagonal = new Vector(getVectors()[0], getVectors()[2]);

        return diagonal.scale(0.5).add(getVectors()[0]);
    }
}
