package tracketeer.search.potfield.visual;

import tracketeer.search.geom2d.DrawableShape;
import tracketeer.search.geom2d.Line;
import tracketeer.search.geom2d.Rect;
import tracketeer.search.geom2d.Vector;
import tracketeer.search.potfield.PotentialField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Iterator;
import java.util.List;

public class JPotField extends JComponent {

    private PotentialField field;

    public JPotField(PotentialField field) {
        this.field = field;

        addKeyListener(new Controls());
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        Rectangle rect = getBounds();
        g.setColor(new Color(80, 130, 50));
        g.fillRect(rect.x, rect.y, rect.width, rect.height);

        paintObstacles(g);
        paintTarget(g);
        paintScout(g);

        paintHitPoints(g);
        paintPotential(g);

        Iterator<Vector> path = field.getPath().iterator();
        if (path.hasNext()) {
            Vector start = path.next();

            g.setColor(Color.BLACK);
            while (path.hasNext()) {
                Vector next = path.next();
                g.drawLine(start.x, start.y, next.x, next.y);

                start = next;
            }
        }
    }

    private void paintPotential(Graphics2D g) {
        List<PotentialField.Potential> potentials = field.getPotentials();
        double maxPotential = 0;
        PotentialField.Potential min = null;

        for (PotentialField.Potential pot : potentials) {
            if (pot.getValue() > maxPotential) {
                maxPotential = pot.getValue();
            }
            if (min == null || pot.getValue() < min.getValue()) {
                min = pot;
            }
        }

        for (PotentialField.Potential pot : potentials) {
            double part = (pot.getValue() / maxPotential);
            int red = (int) Math.round(255 * part);
            int green = (int) Math.round(255 * (1 - part));

            g.setColor(new Color(red, green, 0));
            g.fillOval(pot.loc.x - 5, pot.loc.y - 5, 10, 10);
            g.setColor(Color.BLACK);
            g.drawOval(pot.loc.x - 5, pot.loc.y - 5, 10, 10);

            if (pot == min) {
                g.drawOval(pot.loc.x - 3, pot.loc.y - 3, 6, 6);
            }

            Vector v = new Vector(field.getPosition(), pot.loc);
            v.length(field.getSensorRange()).add(field.getPosition());

            g.setColor(new Color(0, 200, 0));
            g.drawString(String.format("%.2f", pot.goal), v.x, v.y);
            g.setColor(new Color(200, 0, 0));
            g.drawString(String.format("%.2f", pot.obstacle), v.x, v.y + g.getFont().getSize());
        }
    }

    private void paintHitPoints(Graphics2D g) {
        List<Vector> hitPoints = field.getHitPoints();
        for (Vector pt : hitPoints) {
            g.setColor(Color.RED);
            g.drawOval(pt.x - 5, pt.y - 5, 10, 10);
            g.drawRect(pt.x, pt.y, 1, 1);
        }
    }

    protected void paintScout(Graphics2D g) {
        g.setColor(Color.GREEN);
        field.getPosition().drawOn(g);
        g.drawOval(
                field.getPosition().x - field.getSampleRange(),
                field.getPosition().y - field.getSampleRange(),
                field.getSampleRange() * 2, field.getSampleRange() * 2);

        g.setColor(new Color(0, 0, 255, 100));
        g.drawOval(
                field.getPosition().x - field.getSensorRange(),
                field.getPosition().y - field.getSensorRange(),
                field.getSensorRange() * 2, field.getSensorRange() * 2);

        g.setColor(Color.RED);
        for (Vector pt : field.getSamplePoints()) {
            Line line = new Line(field.getPosition(), pt).length(field.getSensorRange() - field.getSampleRange());

            line.drawOn(g);
        }
    }

    protected void paintObstacles(Graphics2D g) {
        g.setColor(Color.WHITE);
        for (Shape ob : field.getObstacles()) {
            drawShape(ob, g);
        }
    }

    protected void paintTarget(Graphics2D g) {
        g.setColor(Color.GREEN);
        drawShape(field.getTarget(), g);
    }

    protected void drawShape(Shape shape, Graphics2D g) {
        if (shape instanceof DrawableShape) {
            ((DrawableShape) shape).drawOn(g);
        } else {
            Rectangle rect = shape.getBounds();
            g.drawRect(rect.x, rect.y, rect.width, rect.height);
        }
    }

    public static JFrame showPotentialField(PotentialField field) {
        JPotField pf = new JPotField(field);
        JFrame frame = new JFrame("Showing PotentialField " + field);

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationByPlatform(true);
        frame.setSize(640, 480);
        frame.setContentPane(pf);

        frame.setVisible(true);

        pf.requestFocus();

        return frame;
    }

    public static void main(String[] args) {
        PotentialField field = new PotentialField(new Vector(150, 150), new Rect(340, 400, 20, 20));

        field.getObstacles().add(new Rect(300, 300, 120, 80).rotate(new Vector(300, 300), Math.toRadians(-45)));
        field.getObstacles().add(new Rect(200, 220, 40, 100));
        JPotField.showPotentialField(field).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    class Controls implements KeyListener {

        public void keyTyped(KeyEvent e) {

        }

        public void keyReleased(KeyEvent e) {
        }

        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                double deg = Math.toDegrees(field.getHeading());
                field.setHeading(Math.toRadians(deg - 10));
                repaint();
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                double deg = Math.toDegrees(field.getHeading());
                field.setHeading(Math.toRadians(deg + 10));
                repaint();
            } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                Vector pos = field.getPosition().copy().add(
                        new Vector(field.getPosition(), field.getNose()).length(10));
                field.setPosition(pos);
                repaint();
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                Vector pos = field.getPosition().copy().add(
                        new Vector(field.getPosition(), field.getNose()).length(-10));
                field.setPosition(pos);
                repaint();
            } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                field.nextStep();
                repaint();
            } else if (e.getKeyCode() == KeyEvent.VK_R && e.isControlDown()) {
                field.finalStep();
                repaint();
            }
        }
    }
}
