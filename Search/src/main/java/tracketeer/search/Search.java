package tracketeer.search;

import java.awt.Shape;
import java.util.Set;

public interface Search {
    Step nextStep();
    void setStepSize(int stepSize);
    int getStepSize();

    Set<Shape> getObstacles();

    Step finalStep();
}
